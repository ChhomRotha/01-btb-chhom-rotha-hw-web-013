import React from 'react'

function View(props) {
    return (
        <div>  
            <h3>{props.name}</h3>
        </div>
    )
}

export default View
