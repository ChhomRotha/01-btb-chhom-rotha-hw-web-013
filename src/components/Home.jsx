import React, { Component } from 'react'
import {Nav,Navbar,FormControl,Form,Button,Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import 'moment-timezone';
import Moment from 'react-moment';
import axios from 'axios';
// import View from './View'

export default class Home extends Component {
    state = {
        info: [],
    };

    componentWillMount(){
        console.log("Will Mount");
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{
            console.log(res.data.DATA);
            this.setState({
                info: res.data.DATA,
            });
        });
    }

    myView = () => {
        
    }

    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                <div className="container">  
                <Navbar.Brand>AMS</Navbar.Brand>  
                <Nav className="mr-auto">
                <Nav.Link as = {Link} to ="/home">Home</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-info">Search</Button>
                </Form>
                </div>
                </Navbar>

                <br/>

                <div className="text-center">
                <h1 className="text-center">Article Management</h1>
                <Button variant="outline-info" >
                    <Link to="/add">Add Article</Link>
                </Button>
                </div>

                <br/>

                <div className="container-fliud">
                <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>CREATE DATE</th>
                    <th>IMAGE</th>
                    <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.info.map((infos) => (
                        <tr key={infos.ID}>
                            <td>{infos.ID}</td>
                            <td>{infos.TITLE}</td>
                            <td className="w-50 p-3">{infos.DESCRIPTION}</td>
                            <td>
                                <Moment format="YYYY-MM-DD">{infos.CREATED_DATE.localtime}</Moment>
                            </td>
                            <td><img src={infos.IMAGE} width="120" height="120" alt=""/></td>
                            <td>
                                <Button variant="primary" onClick={() => this.myView()}>View</Button>&nbsp;
                                <Button variant="warning">Edit</Button>&nbsp;
                                <Button variant="danger">Delete</Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
                </Table>
                </div>

            </div>
        );
    }
}
