import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Rotuer, Switch, Route} from 'react-router-dom'
import Home from './components/Home';
import Add from './components/Add';



function App() {
  return (
    <div className="App">
      <Rotuer>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/Home" component={Home}/>
          <Route path="/Add" component={Add}/>
        </Switch>
      </Rotuer>
    </div>
  );
}

export default App;
